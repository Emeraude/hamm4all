import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Gen{

    String name;

    Scanner scan = new Scanner(System.in);

    int wellLength, rep;

    List<Dims> dim;
    List<Laby> laby;

    Gen(){
        this.dim = new ArrayList<Dims>();
        this.laby = new ArrayList<Laby>();

        System.out.println("What is the name of the Land ?");
        this.name = scan.nextLine();
        System.out.println("What is the length of the well ?");
        this.wellLength = scan.nextInt();
        
    }

    public void choose(){
        System.out.println("What would you like to do ?");
        System.out.println("1 - Create a dimension");
        System.out.println("2 - Create a laby");
        System.out.println("3 - Exit");

        rep = scan.nextInt();
        this.sort(rep);
        
    }

    public void sort(int response){

        switch(response){
            case 1:
                this.createDim();
                break;
            case 2:
                this.createLaby();
                break;
            case 3:
                this.generateJSON();
                break;
            default:
                System.out.println("Not good! Please enter again");
                this.choose();
                break;
        }
    }

    public void createDim(){
        dim.add(new Dims());
        this.choose();
    }

    public void generateJSON(){
        System.out.println("    \""+this.name+"\": [");
        for(int i=0 ; i<this.wellLength ; i++){
            System.out.println("        {");
            System.out.println("            \"niveau\": \""+i+"\"");
            System.out.println("        },");
        }
        for(Dims d: dim){
            System.out.println("        {");
            System.out.println("            \"niveau\": \""+d.start+"\",");
            System.out.println("            \"dims\": [");
            System.out.println("               \""+d.start+".0\"");
            System.out.println("            ]");
            System.out.println("        },");
            for(int j=0 ; j<d.nbLevels ; j++){
                System.out.println("        {");
                System.out.println("           \"niveau\": \""+d.start+"."+j+"\"");
                System.out.println("        },");
            }
        }
        for(Laby l: laby){
            System.out.println("        {");
            System.out.println("            \"niveau\": \""+l.start+"\",");
            System.out.println("            \"dims\": [");
            System.out.println("               \""+l.start+".(0;0)\"");
            System.out.println("            ]");
            System.out.println("        },");
            for(int x=l.XStart ; x<=l.XEnd ; x++){
                for(int y=l.YStart ; y<=l.YEnd ; y++){
                    System.out.println("        {");
                    System.out.println("           \"niveau\": \""+l.start+".("+x+";"+y+")\"");
                    System.out.println("        },");
                }
            }
        }
        System.out.println("    ]");
    }

    public void createLaby(){
        laby.add(new Laby());
        this.choose();
    }
}