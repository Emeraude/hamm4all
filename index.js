var express = require('express');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const pug = require('pug');
const fs = require('fs');

const levelsJSON = require('./public/levels.json');
const lands = require("./public/lands.json");
Object.entries(lands).forEach(([k, v]) => v.slug = k);

var app = express();

app.use(express.static('assets'));

/* Index */
app.get('/', function(req, res) { 
    res.render('index.pug')
});

/* Niveaux */
app.get('/contrees/:contree/:curLvl', function(req, res) {
    var partInLvl = false;

    for(var part in lands[req.params.contree]['parts']){
        if(req.params.curLvl == part){
            partInLvl = true;
        }
        if (lands[req.params.contree]['parts'][part]['type'] == 'custom'){
            res.render(req.params.contree+'.pug', {"RETOUR": "/contrees/"+req.params.contree})
        }
    }

    if(partInLvl){

        res.render('templateContree.pug', {"part": req.params.curLvl, "land": req.params.contree, lands, "RETOUR": "/contrees/"+req.params.contree})

    }
    else{

        var lvl = req.params.curLvl.split(".", req.params.curLvl.split(".").length);

        var isLaby = false;

        if(lvl[lvl.length-1].split(";").length == 2){
            isLaby = true;
        }

        var endPart = lvl.splice(-1, 1);
        
        if((req.params.curLvl.split(".", req.params.curLvl.split(".").length)[req.params.curLvl.split(".").length-1]).includes("G") || 
            (req.params.curLvl.split(".", req.params.curLvl.split(".").length)[req.params.curLvl.split(".").length-1]).includes("D") ||
            (req.params.curLvl.split(".", req.params.curLvl.split(".").length)[req.params.curLvl.split(".").length-1]).includes("B") ||
            (req.params.curLvl.split(".", req.params.curLvl.split(".").length)[req.params.curLvl.split(".").length-1]).includes("H")){
            res.redirect('/'+req.params.contree+"/"+req.params.curLvl.substr(0, req.params.curLvl.length-1));
        }
        
        else{
            var firstPart = lvl.join(".");

            if(firstPart !== ""){
                firstPart += ".";
            }

            lvl = req.params.curLvl.split(".", req.params.curLvl.split(".").length);

            var startLvl = lvl[0]

            if(lvl[0].includes("G") || lvl[0].includes("D") || lvl[0].includes("H") || lvl[0].includes("B")){
                startLvl = startLvl.substr(0, startLvl.length-1)
            }

            res.render('_levels.pug', {levels: levelsJSON, lvl, "land": req.params.contree, firstPart, endPart,"startLvl": startLvl, "isLaby": isLaby})
        }
    }
}

);

app.get('/contrees', function(req, res) {
  let sortedLands = Object.values(lands);
  sortedLands.sort((a, b) => (a.difficulty != '?' ? a.difficulty : -1)
                   - (b.difficulty != '?' ? b.difficulty : -1));
  res.render("contrees.pug", {lands: sortedLands});
});

app.get('/:page', function(req, res) {
    res.render(req.params.page+".pug");
});

app.get('/contrees/:contree', function(req, res) {
  let land = lands[req.params.contree]
  if (Object.keys(land.parts).length < 2)
    res.redirect('/contrees/' + land.slug + '/' + Object.keys(land.parts)[0]);
  else
    res.render("menuLand.pug", {"land": land, "RETOUR": "/contrees"});
});

app.get('/:page/:sousPage', function(req, res) {
    var path = req.params.page+"/"+req.params.sousPage;
    try{
        var Json = require("./views/"+path+".json");
    }
    catch{}
    var sousp = req.params.sousPage;
    res.render(path+".pug", {"RETOUR": "/"+req.params.page, Json});
});

app.listen(process.env.PORT || 8080);
