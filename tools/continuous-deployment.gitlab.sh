#!/usr/bin/env bash

# Exit with nonzero exit code if anything fails
set -e

###############################################################################
# Configuration                                                               #
###############################################################################

# Deploy only on merge commit to this branch
MAIN_REPO="FireKing54/hamm4all"
# Deploy only on merge commit to this branch
MAIN_BRANCH="master"

###############################################################################
# Get information about the current build                                     #
###############################################################################

# Get CI-dependent build info

# Build id used for the publication of pre-release builds to `npm`
BUILD_ID="${CI_JOB_ID}"
# Branch name or tag name
GIT_BRANCH="${CI_COMMIT_REF_NAME}"
# If this is build for a tag, name of the tag (empty string otherwise)
GIT_HEAD_TAG="${CI_COMMIT_TAG}"
# Branch name or tag name
GIT_REPO="${CI_PROJECT_PATH}"
# Possible values: "branch", "tag", "pr"
CI_BUILD_TYPE="branch"
if [[ -n "${GIT_HEAD_TAG}" ]]; then
  CI_BUILD_TYPE="tag"
fi

# Get CI-independent build info

# If the current build is a tag build, check if the corresponding tag is on the deployment branch
IS_GIT_HEAD_TAG_ON_MAIN_BRANCH="false"
if [[ "${CI_BUILD_TYPE}" == "tag" ]]; then
  # Revert `--single-branch` (caused by `--depth`)
  git config remote.origin.fetch refs/heads/*:refs/remotes/origin/*
  # Fetch all (TODO: Only fetch the last commits of the main branch)
  if [[ $(git rev-parse --is-shallow-repository) == "true" ]]; then
    git fetch --quiet --unshallow --tags
  else
    git fetch --quiet --tags
  fi
  # List the tags on the deployment branch (ignore errors), use grep to perform an exact match and test if the match returns the tag
  IS_GIT_HEAD_TAG_ON_MAIN_BRANCH="$(! (git tag --merged "origin/${MAIN_BRANCH}" 2> /dev/null || true) | grep --quiet --fixed-strings --line-regexp "${GIT_HEAD_TAG}" && echo "false" || echo "true")"
fi
# Time of the latest commit in seconds since UNIX epoch
GIT_HEAD_TIME="$(git log -1 --pretty=format:%ct)"

echo "config: main repo: ${MAIN_REPO}"
echo "config: main branch: ${MAIN_BRANCH}"
echo "ci: build id: ${BUILD_ID}"
echo "ci: build type: ${CI_BUILD_TYPE}"
echo "git: repo: ${GIT_REPO}"
echo "git: branch: ${GIT_BRANCH}"
echo "git: tag: ${GIT_HEAD_TAG}"
echo "git: is tag on main branch: ${IS_GIT_HEAD_TAG_ON_MAIN_BRANCH}"
echo ""

###############################################################################
# Check if we should deploy                                                   #
###############################################################################

if [[ "${GIT_REPO}" != "${MAIN_REPO}" ]]; then
  echo "Skipping deployment: Current repo ${GIT_REPO} is not the main repo ${MAIN_REPO}."
  exit 0
fi

case ${CI_BUILD_TYPE} in
  "pr")
    echo "Skipping deployment: This is only a Merge Request."
    exit 0
    ;;
  "tag")
    # Only commits to the source branch should deploy
    if [[ "${IS_GIT_HEAD_TAG_ON_MAIN_BRANCH}" != "true" ]]; then
      echo "Skipping deployment: Current tag ${GIT_HEAD_TAG} is not on the deployment branch ${MAIN_BRANCH}."
      exit 0
    fi
    ;;
  "branch")
    # Only commits to the source branch should deploy
    if [[ "${GIT_BRANCH}" != "${MAIN_BRANCH}" ]]; then
        echo "Skipping deployment: Not on the deployment branch ${MAIN_BRANCH}."
        exit 0
    fi
    ;;
esac

###############################################################################
# Deployment info                                                             #
###############################################################################

echo "+------------------------------------------------------------+"
echo "| Deploying release                                          |"
echo "+------------------------------------------------------------+"
echo ""

###############################################################################
# npm deployment                                                              #
###############################################################################

echo "Deploying..."

PROJECT_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"
cd "${PROJECT_ROOT}"
./tools/deploy.sh

echo "Successfully deployed"
