#!/usr/bin/env bash
set -e

if [[ -z "${ENC_KEY}" ]]; then echo "Missing variable: ENC_KEY"; exit 1; fi
if [[ -z "${ENC_IV}" ]]; then echo "Missing variable: ENC_IV"; exit 1; fi

PROJECT_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"
cd "${PROJECT_ROOT}"

mkdir -p ~/.ssh
echo "eternalfest.net ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPEzB40+IOnKmZHGLHtOa3cLXK4GuRohyEUtUlivsA29jHCDKwyJmBy/caBIQxPXG8oT6pxiHYqC67/jKaa5nqQ=" >> ~/.ssh/known_hosts

openssl aes-256-cbc -d -K "${ENC_KEY}" -iv "${ENC_IV}" -in deploy_key.enc -out deploy_key
eval "$(ssh-agent -s)"
chmod 600 deploy_key
KEY_PATH="${PROJECT_ROOT}/deploy_key"

cd build
rsync -rlv -e "ssh -i \"${KEY_PATH}\"" -- . hamm4all.eternalfest.net@eternalfest.net:~/public
